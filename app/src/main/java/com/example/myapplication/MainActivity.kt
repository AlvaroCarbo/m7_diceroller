package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val rollButton =  findViewById<Button>(R.id.button)
        val resultImageView = findViewById<ImageView>(R.id.dau2)
        val resultImageView1 = findViewById<ImageView>(R.id.dau1)

        rollButton.setOnClickListener {
            val number = randomNumber()
            if (number == 1){
                resultImageView.setBackgroundResource(R.drawable.dice_1)
            } else if (number == 2){
                resultImageView.setBackgroundResource(R.drawable.dice_2)
            } else if (number == 3){
                resultImageView.setBackgroundResource(R.drawable.dice_3)
            }else if (number == 4){
                resultImageView.setBackgroundResource(R.drawable.dice_4)
            }else if (number == 5){
                resultImageView.setBackgroundResource(R.drawable.dice_5)
            }else if (number == 6){
                resultImageView.setBackgroundResource(R.drawable.dice_6)
            }

            val number1 = randomNumber()
            if (number1 == 1){
                resultImageView1.setBackgroundResource(R.drawable.dice_1)
            } else if (number1 == 2){
                resultImageView1.setBackgroundResource(R.drawable.dice_2)
            } else if (number1 == 3){
                resultImageView1.setBackgroundResource(R.drawable.dice_3)
            }else if (number1 == 4){
                resultImageView1.setBackgroundResource(R.drawable.dice_4)
            }else if (number1 == 5){
                resultImageView1.setBackgroundResource(R.drawable.dice_5)
            }else if (number1 == 6){
                resultImageView1.setBackgroundResource(R.drawable.dice_6)
            }

            if (number == 6 && number1 == 6){
                val text = "JACKPOT"
                val duration = Toast.LENGTH_SHORT

                val toast = Toast.makeText(applicationContext, text, duration)
                toast.show()
            }
        }
    }

    private fun randomNumber () : Int{
        return (1 until 7).random()
    }
}